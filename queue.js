let collection = [];

// // Write the queue functions below.

function print() {
	return collection
} 

function enqueue(element){
	collection.push(element)
	return collection
}

function dequeue(){
	collection.shift()
	return collection
}

function front(){
    let first = collection[0]
    return first
}

function size(){
	let number = collection.length;
	return number	
}

function isEmpty(){
	let element = collection.length;

	if(element !== 0){
		return false
	}
	
}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};

